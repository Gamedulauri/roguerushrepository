﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour {

    public float fireRate = 3f;
    public GameObject bullet;
   

    void Start()
    {
        InvokeRepeating("Spawn", fireRate, fireRate);
      

    }

    void Spawn()
    {

        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        Instantiate(bullet, transform.position, transform.rotation);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
