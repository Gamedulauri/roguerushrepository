﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairScript : MonoBehaviour {
    public LayerMask whatIsPlayer;

    public float playerDetectionRange;
    public float speed;

    private void Start()
    {
        playerDetectionRange = GameObject.FindWithTag("Player").GetComponent<PlayerScript>().enemyDetectionRange;

    }

    private void Update()
    {
        EnableCrosshair();
    }

    bool IsPlayerNear()
    {
        if (Physics2D.OverlapCircle(transform.position, playerDetectionRange, whatIsPlayer))
        {
            return true;
        }
        return false;
    }
    
    void EnableCrosshair()
    {
        if(IsPlayerNear() == true)
        {
            if (transform.localScale.x < 1)
            {
                transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * speed, 
                    transform.localScale.y + Time.deltaTime * speed, transform.localScale.z + Time.deltaTime * speed);
            }
        }
        else if (IsPlayerNear() == false)
        {

            if (transform.localScale.x > 0)
            {
                transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime * speed,
                    transform.localScale.y - Time.deltaTime * speed, transform.localScale.z - Time.deltaTime * speed);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.position, playerDetectionRange);
    }

}
