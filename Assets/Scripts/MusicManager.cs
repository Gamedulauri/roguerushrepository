﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour {

    string tF;
    AudioSource audio;
    public AudioClip mainmenuMusic;
    public AudioClip level1Music;
    public AudioClip level2Music;
    public AudioClip level3Music;

    void OnLevelWasLoaded(int level)
    {
        
        tF = PlayerPrefs.GetString("Mute");

        AudioSource audio = GetComponent<AudioSource>();
        

        if (level == 1 && audio.clip != mainmenuMusic)
        {
            audio.Stop();
            audio.clip = mainmenuMusic;
            audio.Play();
        }
        else
        {

        }
        if (level == 4)
        {
            audio.Stop();
            audio.clip = level1Music;
            audio.Play();

        }
        if (level == 5)
        {
            audio.clip = level2Music;
            audio.Play();

        }
        if (level == 6)
        {
            audio.clip = level3Music;
            audio.Play();

        }
    }

    void Update()
    {
        
        tF = PlayerPrefs.GetString("Mute");
        AudioSource audio = GetComponent<AudioSource>();
        if (Time.timeScale == 0)
        {
            audio.Pause();
        }
        else
        {
            audio.UnPause();
        }

        if (tF == "true")
        {
            audio.volume = 1;
        }
        else if (tF == "false")
        {
            audio.volume = 0;
        }
    }
    void Start ()
    {
        PlayerPrefs.SetString("Mute", "true");
        Debug.Log(tF);
    }

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        
    }
}
