﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SettingManager : MonoBehaviour {

    public bool musicVolume = true;
    public Button applyButton;

    public AudioSource musicSource;
    public GameSettings gameSettings;

    void OnEnable()
    {
        gameSettings = new GameSettings();
        applyButton.onClick.AddListener(delegate { OnApplyButtonClick(); });

    }

    public void OnMusicVolumeChange()
    {
        musicVolume = !musicVolume;
            if (musicVolume)
            {
                musicSource.Play();
                //audioSource.mute = true;
                //audioSource.volume = 1.0f;
            }
            else
            {
            musicSource.Stop();
                //audioSource.mute = false;
                //audioSource.volume = 0.0f;
            }
        }

    public void OnApplyButtonClick()
    {
        SaveSettings();
    }

    public void SaveSettings()
    {
        string jsonData = JsonUtility.ToJson(gameSettings, true);
        File.WriteAllText(Application.persistentDataPath + "/gamesettings.json", jsonData);
    }

    public void LoadSettings()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesettings.json") == true)
        {
            LoadSettings();
        }
    }
}

