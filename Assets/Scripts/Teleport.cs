﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    public Transform warpTarget;


void OnTriggerEnter2D(Collider2D other)
    {
        //Makes the collided object transform to the defined target object
        other.gameObject.transform.position = warpTarget.position;

        Debug.Log("An object collided with teleporter and was sent to " + warpTarget.position);
    }
}
