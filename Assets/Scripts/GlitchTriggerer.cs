﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchTriggerer : MonoBehaviour
{

    GameObject player;
    GameObject bottomHitbox;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        bottomHitbox = GameObject.FindWithTag("ReloadBox");
    }

    void Update()
    {
        if (player.GetComponent<PlayerScript>().WallDetection() == true || bottomHitbox.GetComponent<RestartLevelWhenHit>().CollisionDetection() == true)
        {
            TriggerGlitch();
        }
    }

    public void TriggerGlitch()
    {
        if (GetComponent<Kino.AnalogGlitch>().colorDrift < 1)
        {
            GetComponent<Kino.AnalogGlitch>().colorDrift = GetComponent<Kino.AnalogGlitch>().colorDrift + Time.deltaTime;
        }
        if (GetComponent<Kino.AnalogGlitch>().scanLineJitter < 0.1f)
        {
            GetComponent<Kino.AnalogGlitch>().scanLineJitter = GetComponent<Kino.AnalogGlitch>().scanLineJitter + Time.deltaTime;
        }
        if (GetComponent<Kino.AnalogGlitch>().verticalJump < 0.05f)
        {
            GetComponent<Kino.AnalogGlitch>().verticalJump = GetComponent<Kino.AnalogGlitch>().scanLineJitter + Random.Range(-0.03f, 0.03f);
        }
        if (GetComponent<Kino.DigitalGlitch>().intensity < 0.01f)
        {
            GetComponent<Kino.DigitalGlitch>().intensity = GetComponent<Kino.DigitalGlitch>().intensity + Time.deltaTime;
        }

    }
}
