﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Destroys the gameobject when collided with player
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
        }
    }

}