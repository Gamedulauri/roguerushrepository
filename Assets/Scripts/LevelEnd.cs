﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;

public class LevelEnd : MonoBehaviour {
    public int levelNumber = 1;

    [Space]

    public GameObject endMenu;
    public GameObject pauseButton;
    string tF;

    void Start()
    {
        endMenu.SetActive(false);
        Advertisement.Initialize("1595975");

        Analytics.CustomEvent("level_start", new Dictionary<string, object>
        {
            {"level_index", levelNumber}
        });
    }

    void OnTriggerStay2D(Collider2D other)
    {

        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        Advertisement.Show();
        endMenu.SetActive(true);
        pauseButton.SetActive(false);
        Time.timeScale = 0;
        Debug.Log("Ad is shown.");
        
        if (sceneName == "Level1")
        {
            PlayerPrefs.SetString("Level1Clear", "Clear");
        }
        else if (sceneName == "Level2")
        {
            PlayerPrefs.SetString("Level2Clear", "Clear");
        }

        else if (sceneName == "Level3")
        {
            PlayerPrefs.SetString("Level3Clear", "Clear");
        }

        Analytics.CustomEvent("level_complete", new Dictionary<string, object>
        {
            {"level_index", levelNumber}
        });
    }
}
