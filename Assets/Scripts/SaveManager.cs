﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour {

    //www.youtube.com /watch?v=FRKYVhxzrRw

    public static SaveManager Instance { set; get; }
    public SaveState state;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
        Load();

        Debug.Log(SaveHelper.Serialize<SaveState>(state));
    }

    //Tallentaa staten playerprefsille
    public void Save()
    {
        PlayerPrefs.SetString("save", SaveHelper.Serialize<SaveState>(state));

    }

    //Lataa kaiken
    public void Load()
    {
        if (PlayerPrefs.HasKey("save"))
        {
            state = SaveHelper.Deserialize<SaveState>(PlayerPrefs.GetString("save"));
        }
        else
        {
            state = new SaveState();
            Save();
            Debug.Log("Ei aijempaa tallennusta, uusi tallennus tehty.");
        }
    }

    public bool IsCollectableGotten(int index)
    {
        return (state.collectableGot & (1 << index)) != 0;
    }

    public void CollectCollectable(int index)
    {
        state.collectableGot |= 1 << index;
    }

    //Poistaa tallennuksen
    public void ResetSave()
    {
        PlayerPrefs.DeleteKey("save");
    }
}
