﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgReset : MonoBehaviour {
    string lvl1;
    string lvl2;
    string lvl3;

    public Text txt;
    public Text txt2;
    public Text txt3;

    void Start () {
        lvl1 = PlayerPrefs.GetString("Level1Clear");
        lvl2 =PlayerPrefs.GetString("Level2Clear");
        lvl3 = PlayerPrefs.GetString("Level3Clear");
    }

    void Update()
    {
        lvl1 = PlayerPrefs.GetString("Level1Clear");
        lvl2 = PlayerPrefs.GetString("Level2Clear");
        lvl3 = PlayerPrefs.GetString("Level2Clear");
        txt2.text = "Playerprefs: " + "\n" + "Level1Clear = " + lvl1 + "\n" + "Level2Clear = " + lvl2 + "\n" + "Level3Clear = " + lvl3;
    }
    
    public void Reset()
    {
        PlayerPrefs.SetString("Level1Clear", "Not Clear");
        PlayerPrefs.SetString("Level2Clear", "Not Clear");
        PlayerPrefs.SetString("Level3Clear", "Not Clear");
        txt.text = "Level progression is now reseted!";
    }

    public void Clear()
    {
        PlayerPrefs.SetString("Level1Clear", "Clear");
        PlayerPrefs.SetString("Level2Clear", "Clear");
        PlayerPrefs.SetString("Level3Clear", "Clear");
        txt.text= "Level progression is now set to 'Clear'!";
    }
}
