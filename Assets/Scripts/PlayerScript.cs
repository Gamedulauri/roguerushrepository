﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    bool customEvent = true;


    [Tooltip("moves dash to update when turned on. Use on computer builds. Turn off for mobile builds")]
    public bool DEBUGMODE = false;

    [Tooltip("What is marked as a platform that the player can jump on")]
    public LayerMask whatIsGround;
    [Tooltip("What is marked as an enemy and therefor is dashable to")]
    public LayerMask whatIsEnemy;
    
    // Movement speed
    [Tooltip("The player's running speed")]
    public float speed = 20;

    // Jump force
    [Tooltip("How much force does the jump have ie. How high does the player jump")]
    public float force = 30;

    private bool hasDashed;
    private bool hasDashedToEnemy;

    Rigidbody2D rb;

    //Dash speed
    [Tooltip("Distance of where the player can detect an enemy from")]
    public float enemyDetectionRange = 5f;
    [Tooltip("The speed of the players dash")]
    public float dashSpeed = 1f;

    private float dashTimer;
    public float defaultDashTimer = 1f;
    private float afterDashTimer;
    public float defaultAfterDashTimer = 1f;

    //Dash duration
    [Tooltip("Determines the duration of a dash")]
    public float defaultTimer = 1f;

    //How far can the player register a wall in front of him/her from
    [Tooltip("How far can the player register a wall in front of him/her from")]
    public float WallDetectionRange = 0.3f;

    //Death delay timer
    [Tooltip("The current load after death")]
    public string currentLevel;
    [Tooltip("Determines how long it takes for the level to restart after hitting a wall")]
    public float restartTimer = 1f;
    private float trailDisableTimer = 0.1f;
    SpriteRenderer _sr;

    GameObject reloadBox;

    [Tooltip("Audio for dying")]
    public AudioClip deathSound;

    public Image progressionbar;

    public GameObject levelEnd;

    AudioSource audio;
    bool hasPlayed;
    bool playerAlive = true;

    bool dashHold;
    public int dashHoldRange;
    //Sets animations
    Animation anim;


    void Start()
    {
        GetComponent<DeathPosEvent>();

        //Sets the player's rigidbody
        rb = GetComponent<Rigidbody2D>();

        //Sets the player's animation
        anim = GetComponent<Animation>();

        _sr = GetComponent<SpriteRenderer>();
        
        audio = GetComponent<AudioSource>();
        hasPlayed = false;

        //Triggers the default running animation
        anim.Play("characterRunAnimation");

        hasDashed = false;
        hasDashedToEnemy = false;
        afterDashTimer = defaultAfterDashTimer;

        reloadBox = GameObject.FindWithTag("ReloadBox");

        dashHold = false;

    }

    private void Update()
    {
        if (playerAlive)
        {
            Climb();
            Jump();
            if (DEBUGMODE == true && WallDetection() == false)
            {
                NewDash();
            }
        }
        else
        {
            

            restartTimer -= Time.deltaTime;

            if (restartTimer <= 0)
            {   //Restart the level when countdown reaches 0
                SceneManager.LoadScene(currentLevel);
            }

        }

        if (Input.GetButtonUp("Fire1") && hasDashed == true)
        {
            rb.velocity = new Vector2((Vector2.right * speed).x, rb.velocity.y);
        }
    }

    void FixedUpdate()
    {
        //Calls methods
            RunRight();

        Climb();
        if (DEBUGMODE == false && WallDetection() == false)
        {
            NewDash();
        }

        //if player has hit a wall: restart scene
        if (WallDetection() == true ||
            reloadBox.GetComponent<RestartLevelWhenHit>().CollisionDetection() == true)
        {
            LevelRestart();

        }
        progressionbar.fillAmount = transform.position.x / levelEnd.transform.position.x;
    }

    void RunRight()
    {
        if (hasDashed == false && IsGrounded() == true || dashHold == false)
        {
            Vector2 temp = new Vector2();
            // Run towards the right

            temp.x = (Vector2.right * speed).x;
            temp.y = rb.velocity.y;

            rb.velocity = temp;
        }
    }

    public void Climb()
    {
        Vector2 raycastPosDown = new Vector2(transform.position.x, transform.position.y - 0.5f);

        if (Physics2D.Raycast(raycastPosDown, Vector2.right * WallDetectionRange, WallDetectionRange, whatIsGround))
        {
            Vector2 direction = new Vector2(transform.position.x - 10, transform.position.y + 10);
            rb.AddForce(direction * 10);
        }

    }

    public bool WallDetection()
    {   //Defines the positions of the rightside raycasts that check if player has hit a wall
        Vector2 raycastPosUp = new Vector2(transform.position.x, transform.position.y + 1f);


        //Casts 2 raycasts that check if player has hit a wall
        if (Physics2D.Raycast(raycastPosUp, Vector2.right * WallDetectionRange, WallDetectionRange, whatIsGround))
        {
            return true;
        }

        return false;
    }

    public void LevelRestart()
    {
        playerAlive = false;
     if (hasPlayed == false)
        {
            hasPlayed = true;
            audio.PlayOneShot(deathSound, 0.75f);
            
        }

     if (customEvent == true)
        {
            GetComponent<DeathPosEvent>().CustomEvent();
            customEvent = false;
        }


     //Freezes the player
        rb.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezePositionX
            | RigidbodyConstraints2D.FreezeRotation;

        

        //Makes player invisible
        _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b, 0f);

        //Gradually removes the rail after the player has stopped
        GetComponent<GhostSprites>().ClearTrail();
        trailDisableTimer -= Time.deltaTime;

        if (trailDisableTimer <= 0)
        {   //After the trail is gone, disable the instantiation of a new trail
            GetComponent<GhostSprites>().KillSwitchEngage();
        }
    }
    
    bool IsGrounded()
    {   //Defines the positions of the bottom raycasts that check if player is on the ground
        Vector2 raycastPosLeft = new Vector2(transform.position.x - 1f, transform.position.y);
        Vector2 raycastPosRight = new Vector2(transform.position.x + 1f, transform.position.y);

        //Casts 2 raycasts that check if player is on the ground
        if (Physics2D.Raycast(raycastPosLeft, Vector2.down, 1.2f, whatIsGround) ||
            Physics2D.Raycast(raycastPosRight, Vector2.down, 1.2f, whatIsGround))
        {
            //if player is grounded, this makes the player to be able to dash again
            hasDashed = false;
            hasDashedToEnemy = false;
            
            dashTimer = defaultDashTimer;

            dashHold = false;

            return true;

        }

        return false;
    }

    private void OnCollisionEnter2D(Collision2D col)
    { 

        //Triggers the default running animation when player collides with something
        //COULD/SHOULD BE RELOCATED TO SOMEWHERE MORE SENSIBLE
        anim.Stop();
        anim.Play("characterRunAnimation");

    }

    void Jump()
    {

        if (EventSystem.current.IsPointerOverGameObject(0))
        {
            // ui touched
        }

        else if (Input.GetButtonDown("Fire1") && IsGrounded() == true && Time.timeScale == 1)
        {

            //Makes the player jump upwards
            rb.AddForce(Vector2.up * force, ForceMode2D.Impulse);



            //Triggers a jumping animation
            anim.Stop();
            anim.Play("characterJumpAnimation");

        }
        else if (Input.GetButtonUp("Fire1"))
        {
            dashHold = true;
        }
    }

    public bool IsEnemyNear()
    {   //Checks if enemies are in the players "detection radius"
        if (Physics2D.OverlapCircle(transform.position, enemyDetectionRange, whatIsEnemy))
        {
            return true;
        }
        return false;
    }

    public Collider2D FindClosestEnemy()
    {                                                       //Checks if enemies are in players range
        Collider2D[] Colliders = Physics2D.OverlapCircleAll(transform.position, enemyDetectionRange, whatIsEnemy);
        Collider2D closestCollider = null;

        //Checks for unregistered enemies
        if(Colliders != null && Colliders.Length > 0)
        {
            //if no enemies are nearby: Sets the Colliders -array length to 0
            closestCollider = Colliders[0];

            foreach (Collider2D col2D in Colliders)
            {
                // if enemies are nearby: Checks what enemy is the closest to the player and marks it as the closest enemy
                if (Vector2.Distance (transform.position, col2D.transform.position) < Vector2.Distance(transform.position,col2D.transform.position))
                {
                    closestCollider = col2D;
                }
            }
        }
        //Returns the closest enemy
        return closestCollider;
    }

    private void OnDrawGizmosSelected()
    {
        //Draws visualized version of the enemy detction range in red to Unity editor
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, enemyDetectionRange);

        //Draws visualized version of the wall detection range in blue to Unity editor
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, Vector2.right * WallDetectionRange);

    }

    void NewDash()
    {
        if (EventSystem.current.IsPointerOverGameObject(0))
        {
            // ui touched
        }
        //Checks if the player is able to dash to an enemy
        else if (Input.GetButtonDown("Fire1") && IsEnemyNear() == true && Time.timeScale == 1 && IsGrounded() == false)
        {
            //Checks if player is still in dash state, if not, the timer resets and the player is able to dash again
            if (afterDashTimer <= 0)
            {
                afterDashTimer = defaultAfterDashTimer;
            }
            //Sets the dash's duration
            dashTimer = defaultDashTimer;

            rb.velocity = (FindClosestEnemy().transform.position - transform.position).normalized * dashSpeed;

            //Disables the player from being able to dash again in the same jump
            hasDashed = true;
            hasDashedToEnemy = true;

            //Triggers the dashing animation
            anim.Stop();
            anim.Play("characterDashAnimation");
        }
        else if (Input.GetButton("Fire1") && IsGrounded() == false && hasDashed == false && dashHold == true && dashTimer > 0f)
        {
            dashTimer -= Time.deltaTime * 0.2f;

            //Sets the dash's duration
            //timer = defaultTimer;

            rb.velocity = new Vector2(dashHoldRange, -3);

            //rb.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;

            //Disables the player from being able to dash again in the same jump
            //hasDashed = true;
            dashHold = true;
            //Triggers the dashing animation
            anim.Stop();
            anim.Play("characterDashAnimation");

        }

        if (hasDashedToEnemy == true && dashTimer > 0)
        {
            dashTimer -= Time.deltaTime;
        } //freezes the player for a while after a dash to enemy
        else if (hasDashedToEnemy == true && dashTimer <= 0)
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezePositionX |
RigidbodyConstraints2D.FreezeRotation;
        } 


        if (afterDashTimer > 0)
        {
            afterDashTimer -= Time.deltaTime;
        }
        else
        {
            //Returns the default constraints and running speed after the dash
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;

            if (!Input.GetButton("Fire1") || hasDashedToEnemy)
            {
                rb.velocity = new Vector2(speed, rb.velocity.y);
            }
        }


    }

   
}