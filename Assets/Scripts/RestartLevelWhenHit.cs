﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevelWhenHit : MonoBehaviour {

    [Tooltip("The current load after death")]
    public string currentLevel;

    GameObject player;

    bool hasCollided;

    private void Start()
    {   
        //Defines player
        player = GameObject.FindWithTag("Player");

        hasCollided = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Reloads level when collided with player
        if (collision = player.GetComponent<BoxCollider2D>())
        {
            hasCollided = true;
        }
    }

    public bool CollisionDetection()
    {
        if(hasCollided == true)
        {
            return true;
        }
        return false;
    }

}